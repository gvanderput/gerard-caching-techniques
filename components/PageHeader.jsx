import { Message } from "semantic-ui-react";

export default function PageHeader({ title, description }) {
  return (
    <Message positive>
      <Message.Header>{title}</Message.Header>
      <p>{description}</p>
    </Message>
  );
}
