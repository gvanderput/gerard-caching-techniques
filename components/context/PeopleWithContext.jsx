import { useContext } from "react";
import PeopleRenderer from "../PeopleRenderer";
import { PeopleContext } from "./context";

export default function PeopleWithContext() {
  const { json } = useContext(PeopleContext);

  return <PeopleRenderer json={json} />;
}
