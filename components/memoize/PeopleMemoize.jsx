import { useEffect, useState } from "react";
import getData from "./getData";
import PeopleRenderer from "../PeopleRenderer";

export default function PeopleMemoize() {
  const [json, setJson] = useState(null);

  useEffect(() => {
    getData().then((data) => setJson(data));
  }, []);

  return <PeopleRenderer json={json} />;
}
