import { useEffect, useState } from "react";
import PeopleRenderer from "../PeopleRenderer";

export default function PeopleUseEffect() {
  const [json, setJson] = useState(null);

  useEffect(() => {
    fetch("/api/people")
      .then((response) => response.json())
      .then((data) => setJson(data));
  }, []);

  return <PeopleRenderer json={json} />;
}
