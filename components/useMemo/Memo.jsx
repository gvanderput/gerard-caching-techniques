import { useCallback, useEffect, useMemo, useState } from "react";
import { Button, Label } from "semantic-ui-react";

export default function Memo() {
  const getRnd = () => Math.round(Math.random() * 10000);

  const [age, setAge] = useState(24);
  const [randomNumber, setRandomNumber] = useState(getRnd());

  const pow = useMemo(() => Math.pow(age, 2) + getRnd(), [age]);

  return (
    <>
      {[
        { name: "age", value: age },
        { name: "randomNumber", value: randomNumber },
        { name: "pow", value: pow },
      ].map((prop) => (
        <div className="labelWrapper" key={prop.name}>
          <Label>
            {`${prop.name}:`}
            <Label.Detail>{prop.value}</Label.Detail>
          </Label>
        </div>
      ))}
      <Button onClick={() => setRandomNumber(getRnd())}>
        update randomNumber
      </Button>
      <Button onClick={() => setAge((a) => ++a)}>update age</Button>
    </>
  );
}
