import "semantic-ui-css/semantic.min.css";
import { Container, Segment } from "semantic-ui-react";
import MainMenu from "../components/MainMenu";
import "../styles/globals.scss";

function MyApp({ Component, pageProps }) {
  return (
    <Container className="mainContainer">
      <MainMenu />
      <Segment>
        <Component {...pageProps} />
      </Segment>
    </Container>
  );
}

export default MyApp;
