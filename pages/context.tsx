import { useEffect, useState } from "react";
import { Button } from "semantic-ui-react";
import PageHeader from "../components/PageHeader";
import PeopleWithContext from "../components/context/PeopleWithContext";
import { PeopleContext } from "../components/context/context";

export default function Context() {
  const [amount, setAmount] = useState(0);

  const [json, setJson] = useState(null);
  const [seed, setSeed] = useState(Math.random());

  useEffect(() => {
    fetch("/api/people")
      .then((response) => response.json())
      .then((data) => setJson(data));
  }, [seed]);

  return (
    <PeopleContext.Provider value={{ json }}>
      <PageHeader
        title="context.tsx"
        description="Fetch data with useEffect and store it with useState, and inject with Context Provider."
      />
      {Array.from(Array(amount).keys()).map((i) => (
        <PeopleWithContext key={i} />
      ))}
      <Button onClick={() => setAmount((a) => ++a)}>Add component</Button>
      <Button onClick={() => setSeed(Math.random())}>Set new seed</Button>
      <Button onClick={() => setAmount(0)}>Reset</Button>
    </PeopleContext.Provider>
  );
}
